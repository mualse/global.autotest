package testPackage;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BaseWebTest {

	WebDriver driver;

	public WebDriver getDriver() {
		return driver;
	}
	
	@Before
	public void setUp() throws Exception
	{
		driver = new FirefoxDriver();
	}

	@After
	public void tearDown() throws Exception
	{
		driver.quit();
	}
}
