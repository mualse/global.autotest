package testPackage;
import homePageObjects.Advanced;
import homePageObjects.Browsers;
import homePageObjects.RepeatViews;
import homePageObjects.TestLocations;
import homePageObjects.TestSettings;

import java.util.*;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.*;

import pages.HomePage;
import pages.ResultPage;

@RunWith(Parameterized.class)
public class InteviewTest extends BaseWebTest {

	private final String webSiteUrl;
	private final String testLocation;
	private final String browser;
	private final TestSettings testSettings;
	private final Advanced advanced;
	private final List<String> headingDetails;
	
	public InteviewTest(String webSiteUrl, String testLocation, String browser, TestSettings testSettings, Advanced advanced, List<String> headingDetails) {
		super();
		this.webSiteUrl = webSiteUrl;
		this.testLocation = testLocation;
		this.browser = browser;
		this.testSettings = testSettings;
		this.advanced = advanced;
		this.headingDetails = headingDetails;
	}

	@Parameters
	public static Collection<Object[]> data() {
		List<Object[]> result = new ArrayList<Object[]>();
		TestSettings testSettings = new TestSettings();
		Advanced advanced = new Advanced();
		result.add(new Object[] { "mail.ru", TestLocations.Moscov, Browsers.Firefox, testSettings, advanced, Arrays.asList("Moscow, Russia", "Firefox", "Cable") });
		testSettings.RepeatView = RepeatViews.ViewFirst;
		result.add(new Object[] { "ya.ru", TestLocations.Moscov, Browsers.Firefox, testSettings, advanced, Arrays.asList("Moscow, Russia", "Firefox", "Cable") });
		result.add(new Object[] { "ya.ru", TestLocations.London, Browsers.IE8, testSettings, advanced, Arrays.asList("London, UK - IE8", "Cable") });
		return result;
	}
	
	@Test	
	public void Test() throws Exception 
	{
		// Arrange
		WebDriver driver = getDriver();

		// Act
		driver.get(HomePage.BASE_URL);
		HomePage homePage = new HomePage(driver);

		homePage.setWebSiteUrl(this.webSiteUrl);
		homePage.setTestLocation(this.testLocation);
		homePage.setBrowser(this.browser);
		homePage.expandAdvancedSettings();
		homePage.setTestSettings(this.testSettings);
		homePage.setAdvanced(this.advanced);
		
		homePage.clickStartTestButton();

		ResultPage resultPage = new ResultPage(driver);

		// Assert
		Assert.assertEquals(this.webSiteUrl, resultPage.getHeadingUrl());
		Assert.assertEquals(this.headingDetails, resultPage.getHeadingDetails());
		Assert.assertEquals(testSettings.RepeatView == RepeatViews.ViewFirst ? 1 : 2, resultPage.getRowsNumberOfTableResults());
	}
}
