package pages;
import homePageObjects.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends PageObject
{	
	public static final String BASE_URL = "http://www.webpagetest.org/";
	
	public HomePage(WebDriver driver)
	{
		super(driver, BASE_URL);
	}

	public void setWebSiteUrl(String url) 
	{
		EnterTextBoxValue(By.name("url"), url);
	}

	public void setTestLocation(String where) 
	{
		EnterComboBoxValue(By.name("where"), where);
	}

	public void setBrowser(String browser) 
	{
		EnterComboBoxValue(By.name("browser"), browser);
	}
	
	public void expandAdvancedSettings() 
	{
		GetWebElementBy(By.className("arrow")).click();
	}
	
	public void setTestSettings(TestSettings settings) 
	{
		GetWebElementBy(By.linkText("Test Settings")).click();
		setTestSettingsConnection(settings.Connection);
		setTestSettingsNumberOfTestsToRun(settings.NumberOfTestsToRun);
		setTestSettingsRepeatView(settings.RepeatView);
		setTestSettingsKeepTestPrivate(settings.KeepTestPrivate);
		setTestSettingsLabel(settings.Label);
	}
	
	private void setTestSettingsConnection(String connection) 
	{
		EnterComboBoxValue(By.name("location"), connection);
	}

	private void setTestSettingsNumberOfTestsToRun(String numberOfTestsToRun) 
	{
		EnterTextBoxValue(By.name("runs"), numberOfTestsToRun);
	}

	private void setTestSettingsRepeatView(RepeatViews repeatView) 
	{
		switch(repeatView)
		{
			case ViewBoth:
				SetRadioButton(By.id("viewBoth"));
				break;
			
			case ViewFirst:
				SetRadioButton(By.id("viewFirst"));
				break;
		}
	}

	private void setTestSettingsKeepTestPrivate(boolean keepTestPrivate) 
	{
		SetCheckboxValue(By.name("private"), keepTestPrivate);
	}

	private void setTestSettingsLabel(String label) 
	{
		EnterTextBoxValue(By.name("label"), label);
	}

	public void setAdvanced(Advanced advanced) 
	{
		GetWebElementBy(By.linkText("Advanced")).click();
		
		setStopTestAtDocumentComplete(advanced.StopTestAtDocumentComplete);
		setDisableJavascript(advanced.DisableCompatibilityView);
		setClearSSLCertificateCaches(advanced.ClearSSLCertificateCaches);
		setIgnoreSSLCertificateErrors(advanced.IgnoreSSLCertificateErrors);
		setDisableCompatibilityView(advanced.DisableCompatibilityView);
		setCaptureNetworkPackeTrace(advanced.CaptureNetworkPackeTrace);
		setSaveResponseBodies(advanced.SaveResponseBodies);
		setPreserveOriginalUserAgentString(advanced.PreserveOriginalUserAgentString);
		setDomElement(advanced.DomElement);
		setMinimumTestDuration(advanced.MinimumTestDuration);
	}
	
	
	private void setStopTestAtDocumentComplete(boolean value)
	{
		SetCheckboxValue(By.id("stop_test_at_document_complete"), value);
	}
	
	private void setDisableJavascript(boolean value)
	{
		SetCheckboxValue(By.id("noscript"), value);
	}
	
	private void setClearSSLCertificateCaches(boolean value)
	{
		SetCheckboxValue(By.id("clearcerts"), value);
	}
	
	private void setIgnoreSSLCertificateErrors(boolean value)
	{
		SetCheckboxValue(By.id("ignore_ssl_cerificate_errors"), value);
	}
	
	private void setDisableCompatibilityView(boolean value)
	{
		SetCheckboxValue(By.id("force_standards_mode"), value);
	}
	
	private void setCaptureNetworkPackeTrace(boolean value)
	{
		SetCheckboxValue(By.id("tcpdump"), value);
	}
	
	private void setSaveResponseBodies(boolean value)
	{
		SetCheckboxValue(By.id("bodies"), value);
	}
	
	private void setPreserveOriginalUserAgentString(boolean value)
	{
		SetCheckboxValue(By.id("keepua"), value);
	}
	
	private void setDomElement(String value)
	{
		EnterTextBoxValue(By.id("dom_elements"), value);
	}
	
	private void setMinimumTestDuration(String value)
	{
		EnterTextBoxValue(By.id("time"), value);
	}
	
	public void clickStartTestButton()
	{
		GetWebElementBy(By.className("start_test")).click();
	}
}
