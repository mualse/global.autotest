package pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageObject
{	
	private final WebDriver driver;
	
	public PageObject(WebDriver driver, String baseUrl)
	{
		if (driver == null) 
			throw new IllegalArgumentException("WebDriver cannot be null.");
		this.driver = driver;
		if(!driver.getCurrentUrl().startsWith(baseUrl))
		{
			throw new IllegalStateException("This is not expected page: " + driver.getCurrentUrl());
		}
	}

	protected WebDriver getDriver() {
		return driver;
	}
	
	protected void EnterComboBoxValue(By condition, String value)
	{
		WebElement combobox = GetWebElementBy(condition);
		combobox.sendKeys(value);
	}
	
	protected void EnterTextBoxValue(By condition, String value)
	{
		WebElement textbox = GetWebElementBy(condition);
		textbox.clear();
		textbox.sendKeys(value);
	}
	
	protected void SetRadioButton(By condition)
	{
		WebElement radio = GetWebElementBy(condition);
		radio.click();
	}
	
	protected void SetCheckboxValue(By condition, Boolean value)
	{
		WebElement checkbox = GetWebElementBy(condition);
		if (checkbox.isSelected() != value)
		{
			checkbox.click();
		}
	}
	
	protected WebElement GetWebElementBy(By condition)
	{
		return GetWebElementBy(condition, 10);
	}
	
	protected WebElement GetWebElementBy(By condition, int timeout)
	{
		WebDriverWait wdw = new WebDriverWait(driver, timeout);
		return wdw.until(ExpectedConditions.presenceOfElementLocated(condition));
	}
}
