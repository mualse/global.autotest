package pages;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ResultPage extends PageObject
{	
	public static final String BASE_URL = "http://www.webpagetest.org/result/";
	
	public ResultPage(WebDriver driver)
	{
		super(driver, BASE_URL);
	}
	
	public String getHeadingUrl() {
		return getHeaderData().findElement(By.tagName("h2")).findElement(By.tagName("span")).getText();
	}
	
	public List<String> getHeadingDetails() {
		List<String> result = new ArrayList<String>();
		List<WebElement> list = getHeaderData().findElement(By.className("heading_details")).findElements(By.tagName("b"));
		for (Iterator<WebElement> iterator = list.iterator(); iterator.hasNext();) {
			WebElement webElement = iterator.next();
			result.add(webElement.getText());
		}
		return result;
	}
	
	private WebElement getHeaderData() {
		return GetWebElementBy(By.id("header_data"), 300);
	}
	
	public int getRowsNumberOfTableResults() {
		return GetWebElementBy(By.id("tableResults")).findElements(By.tagName("tr")).size() - 2;
	}
}
