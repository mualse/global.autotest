package homePageObjects;

public class TestSettings
{
	public String Connection;
	public String NumberOfTestsToRun;
	public RepeatViews RepeatView;
	public boolean KeepTestPrivate;
	public String Label;
	
	public TestSettings()
	{
		Connection = Connections.Cabel;
		NumberOfTestsToRun = "1";
		RepeatView = RepeatViews.ViewBoth;
		KeepTestPrivate = false;
		Label = "";
	};
}
