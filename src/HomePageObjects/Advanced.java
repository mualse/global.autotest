package homePageObjects;

public class Advanced
{
	public boolean StopTestAtDocumentComplete;
	public boolean DisableJavascript;
	public boolean ClearSSLCertificateCaches;
	public boolean IgnoreSSLCertificateErrors;
	public boolean DisableCompatibilityView;
	public boolean CaptureNetworkPackeTrace;
	public boolean SaveResponseBodies;
	public boolean PreserveOriginalUserAgentString;
	public String DomElement;
	public String MinimumTestDuration;
	
	public Advanced()
	{
		StopTestAtDocumentComplete = false;
		DisableJavascript = false;
		ClearSSLCertificateCaches = false;
		IgnoreSSLCertificateErrors = false;
		DisableCompatibilityView = false;
		CaptureNetworkPackeTrace = false;
		SaveResponseBodies = false;
		PreserveOriginalUserAgentString = false;
		DomElement = "";
		MinimumTestDuration = "";
	};
}
